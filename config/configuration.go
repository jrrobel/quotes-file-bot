package config

import (
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
)

type Configuration struct {
	DefaultReactionEmoji string
	Homeserver           string
	JoinMessage          string
	StorePath            string

	// Authentication settings
	Username     string
	PasswordFile string
}

func (c *Configuration) GetPassword() (string, error) {
	log.Debug("Reading password from ", c.PasswordFile)
	buf, err := os.ReadFile(c.PasswordFile)
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(string(buf)), nil
}
