package crypto

import (
	mevent "maunium.net/go/mautrix/event"
	mid "maunium.net/go/mautrix/id"
)

type RoomInfoCache struct {
	EncryptionEventCache map[mid.RoomID]*mevent.EncryptionEventContent
	RoomMembership       map[mid.RoomID]map[mid.UserID]struct{}
}

func (ri *RoomInfoCache) SetMembership(roomId mid.RoomID, mids []mid.UserID) {
	for _, userId := range mids {
		if _, found := ri.RoomMembership[roomId]; !found {
			ri.RoomMembership[roomId] = make(map[mid.UserID]struct{})
		}
		ri.RoomMembership[roomId][userId] = struct{}{}
	}
}

func (ri *RoomInfoCache) SetEncryptedEvent(roomId mid.RoomID, encryptionEvent *mevent.EncryptionEventContent) {
	ri.EncryptionEventCache[roomId] = encryptionEvent
}

// IsEncrypted returns whether a room is encrypted.
func (ri *RoomInfoCache) IsEncrypted(roomID mid.RoomID) bool {
	_, found := ri.EncryptionEventCache[roomID]
	return found
}

// GetEncryptionEvent returns the encryption event's content for an encrypted room.
func (ri *RoomInfoCache) GetEncryptionEvent(roomID mid.RoomID) *mevent.EncryptionEventContent {
	encryptionEvent, found := ri.EncryptionEventCache[roomID]
	if !found {
		return nil
	}
	return encryptionEvent
}

// FindSharedRooms returns the encrypted rooms that another user is also in for a user ID.
func (ri *RoomInfoCache) FindSharedRooms(userId mid.UserID) []mid.RoomID {
	rooms := make([]mid.RoomID, 0)
	for roomId, members := range ri.RoomMembership {
		if _, found := members[userId]; found {
			rooms = append(rooms, roomId)
		}
	}
	return rooms
}
