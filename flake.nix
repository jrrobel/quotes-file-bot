{
  description = "Quotesfilebot Development Environment";
  inputs = {
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = inputs@{ nixpkgs-unstable, flake-utils, ... }:
    (flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs-unstable { system = system; };
        in
        {
          devShells.default = pkgs.mkShell {
            packages = with pkgs; [
              go
              gotools
              gopls
              olm
            ];
          };
        }
      ));
}
