package cache

import (
	"errors"
	"fmt"

	"maunium.net/go/mautrix"
	mcrypto "maunium.net/go/mautrix/crypto"
	mevent "maunium.net/go/mautrix/event"
	mid "maunium.net/go/mautrix/id"

	"gitlab.com/jrrobel/quotes-file-bot/helper"
)

type MessageCache struct {
	client     *mautrix.Client
	olmMachine *mcrypto.OlmMachine
	eventCache map[mid.RoomID]map[mid.EventID]*mevent.Event
}

func NewMessageCache(client *mautrix.Client, olmMachine *mcrypto.OlmMachine) *MessageCache {
	return &MessageCache{
		client:     client,
		olmMachine: olmMachine,
		eventCache: make(map[mid.RoomID]map[mid.EventID]*mevent.Event),
	}
}

func (c *MessageCache) GetMessage(roomId mid.RoomID, eventId mid.EventID) (*mevent.Event, error) {
	// Check the cache
	if eventsForRoom, found := c.eventCache[roomId]; found {
		if event, found := eventsForRoom[eventId]; found {
			return event, nil
		}
	}

	rawEvent, err := helper.DoRetry("get raw event", func() (interface{}, error) {
		return c.client.GetEvent(roomId, eventId)
	})
	if err != nil {
		return nil, err
	}
	event := rawEvent.(*mevent.Event)
	event.Content.ParseRaw(event.Type)

	if event.Type == mevent.EventEncrypted {
		decryptedEvent, err := c.olmMachine.DecryptMegolmEvent(event)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("Failed to decrypt %s in %s: %s", eventId, roomId, err))
		}

		event = decryptedEvent
	}

	c.Ingest(roomId, eventId, event)
	return event, nil
}

func (c *MessageCache) Ingest(roomId mid.RoomID, eventId mid.EventID, event *mevent.Event) {
	// If there are more than 100 messages in either level of the cache,
	// just use random eviction because LRU is too hard.
	if len(c.eventCache) > 100 {
		var key mid.RoomID
		for k := range c.eventCache {
			key = k
			break
		}
		delete(c.eventCache, key)
	}
	if len(c.eventCache[roomId]) > 100 {
		var key mid.EventID
		for k := range c.eventCache[roomId] {
			key = k
			break
		}
		delete(c.eventCache[roomId], key)
	}

	if _, found := c.eventCache[roomId]; !found {
		c.eventCache[roomId] = make(map[mid.EventID]*mevent.Event)
	}
	c.eventCache[roomId][eventId] = event
}
