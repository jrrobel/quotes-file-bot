package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"os/signal"
	_ "strconv"
	"syscall"
	"time"

	"github.com/kyoh86/xdg"
	_ "github.com/mattn/go-sqlite3"
	log "github.com/sirupsen/logrus"
	mautrix "maunium.net/go/mautrix"
	mcrypto "maunium.net/go/mautrix/crypto"
	mevent "maunium.net/go/mautrix/event"
	mid "maunium.net/go/mautrix/id"

	"gitlab.com/jrrobel/quotes-file-bot/cache"
	"gitlab.com/jrrobel/quotes-file-bot/config"
	"gitlab.com/jrrobel/quotes-file-bot/crypto"
	"gitlab.com/jrrobel/quotes-file-bot/helper"
	"gitlab.com/jrrobel/quotes-file-bot/store"
	"gitlab.com/jrrobel/quotes-file-bot/workers"
)

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func loadConfig(configPath string) (config.Configuration, error) {
	// load config file and decode into struct
	log.Infof("reading config from %s...", configPath)
	jsonBytes, err := os.ReadFile(configPath)
	if err != nil {
		log.Fatalf("Could not read config from %s: %s", configPath, err)
	}

	var configuration config.Configuration
	err = json.Unmarshal(jsonBytes, &configuration)
	if err != nil {
		log.Fatalf("Error reading config file %s: %s", configPath, err)
	}
	if configuration.StorePath == "" {
		configuration.StorePath = xdg.DataHome() + "/quotesfilebot/store.json"
	}
	return configuration, nil
}

func loadStore(storePath string) (*store.QuotesfilebotStore, error) {
	// load config file and decode into struct
	log.Infof("reading store from %s...", storePath)
	jsonBytes, err := os.ReadFile(storePath)
	if err != nil {
		log.Warningf("Could not read store from %s: %s", storePath, err)
		return store.NewQuotesfilebotStore(), err
	}

	var s store.QuotesfilebotStore
	err = json.Unmarshal(jsonBytes, &s)
	if err != nil {
		log.Warningf("Error reading store file %s: %s", storePath, err)
		return store.NewQuotesfilebotStore(), err
	}
	return &s, nil
}

func saveStore(clientStore *store.QuotesfilebotStore, storePath string) error {
	jsonBytes, err := json.Marshal(clientStore)
	log.Infof("Saving quotesfilebot store to %s", storePath)
	err = os.WriteFile(storePath, jsonBytes, 0600)
	if err != nil {
		return errors.New(fmt.Sprint("Error storing quotesfilebot to %s: %s", storePath, err))
	}
	return nil
}

func main() {
	// Arg parsing
	configPath := flag.String("config", xdg.ConfigHome()+"/quotesfilebot/config.json", "config file location")
	logLevelStr := flag.String("loglevel", "debug", "the log level")
	flag.Parse()

	// Configure logging
	os.MkdirAll(xdg.DataHome()+"/quotesfilebot", 0700)
	logFile, err := os.OpenFile(xdg.DataHome()+"/quotesfilebot/quotesfilebot.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0600)
	if err == nil {
		mw := io.MultiWriter(os.Stdout, logFile)
		log.SetOutput(mw)
	} else {
		log.Errorf("failed to open logging file; using default stderr: %s", err)
	}

	log.SetLevel(log.DebugLevel)
	logLevel, err := log.ParseLevel(*logLevelStr)
	if err == nil {
		log.SetLevel(logLevel)
	} else {
		log.Errorf("invalid loglevel %s. Using default 'debug'.", logLevel)
	}

	log.Info("quotesfilebot starting...")

	// load configuration settings from JSON file
	configuration, err := loadConfig(*configPath)

	// store config options into local variables
	var emojiKey string = configuration.DefaultReactionEmoji
	var username mid.UserID = mid.UserID(configuration.Username)
	_, _, err = username.ParseAndDecode()
	if err != nil {
		log.Fatalf("Could not parse username %s", configuration.Username)
	}

	// load the store
	clientStore, err := loadStore(configuration.StorePath)
	if err != nil {
		log.Warning("Could not load client store.")
	}

	// login to homeserver
	var client *mautrix.Client
	if clientStore.AccessToken != "" {
		client, err = mautrix.NewClient(configuration.Homeserver, username, clientStore.AccessToken)
		if err != nil {
			log.Fatalf("Couldn't login to the homeserver.")
		}
	} else {
		// Use password authentication if we didn't have an access
		// token yet.
		password, err := configuration.GetPassword()
		if err != nil {
			log.Fatalf("Could not read password from %s", configuration.PasswordFile)
			panic(err)
		}
		client, err = mautrix.NewClient(configuration.Homeserver, "", "")
		if err != nil {
			panic(err)
		}
		_, err = helper.DoRetry("login", func() (interface{}, error) {
			return client.Login(&mautrix.ReqLogin{
				Type: mautrix.AuthTypePassword,
				Identifier: mautrix.UserIdentifier{
					Type: mautrix.IdentifierTypeUser,
					User: username.String(),
				},
				Password:                 password,
				InitialDeviceDisplayName: "quotesfilebot",
				StoreCredentials:         true,
			})
		})
		if err != nil {
			log.Fatalf("Couldn't login to the homeserver.")
		}

		clientStore.AccessToken = client.AccessToken
	}

	// set the client store on the client.
	client.Store = clientStore

	// Make sure to exit cleanly
	c := make(chan os.Signal, 1)
	signal.Notify(c,
		os.Interrupt,
		os.Kill,
		syscall.SIGABRT,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGQUIT,
		syscall.SIGTERM,
	)
	go func() {
		for range c { // when the process is killed
			saveStore(clientStore, configuration.StorePath)
			os.Exit(0)
		}
	}()

	go func() {
		saveStore(clientStore, configuration.StorePath)
		time.Sleep(time.Minute)
	}()

	// Setup the crypto store
	db, err := sql.Open("sqlite3", xdg.DataHome()+"/quotesfilebot/crypto.db")
	if err != nil {
		log.Fatal("Could not open crypto database.")
	}
	sqlCryptoStore := mcrypto.NewSQLCryptoStore(
		db,
		"sqlite3",
		username.String(),
		mid.DeviceID("Bot Host"),
		[]byte("quotefilebot_cryptostore_key"),
		crypto.CryptoLogger{},
	)
	err = sqlCryptoStore.CreateTables()
	if err != nil {
		log.Fatal("Could not create tables for the SQL crypto store.")
	}

	roomInfoCache := crypto.RoomInfoCache{
		EncryptionEventCache: make(map[mid.RoomID]*mevent.EncryptionEventContent),
		RoomMembership:       make(map[mid.RoomID]map[mid.UserID]struct{}),
	}
	olmMachine := mcrypto.NewOlmMachine(client, &crypto.CryptoLogger{}, sqlCryptoStore, &roomInfoCache)
	err = olmMachine.Load()
	if err != nil {
		log.Errorf("Could not initialize encryption support. Encrypted rooms will not work.")
	}

	// Find all of the joined rooms.
	j, err := helper.DoRetry("get joined rooms", func() (interface{}, error) {
		return client.JoinedRooms()
	})
	if err != nil {
		log.Fatalf("Couldn't get joined rooms.")
	}
	joinedRoomsResponse := j.(*mautrix.RespJoinedRooms)

	// A set of all of the rooms that the user is in
	joinedRooms := make(map[mid.RoomID]struct{})
	var exists = struct{}{}

	// find the configuration rooms
	configurationRoomForUser := make(map[mid.UserID]mid.RoomID)

	log.Debug("Bot belongs to the following rooms:")
	for i, roomId := range joinedRoomsResponse.JoinedRooms {
		log.Debugf("  %d: %s", i, roomId)
		joinedRooms[roomId] = exists

		mids := make([]mid.UserID, 0)

		membersResp, err := helper.DoRetry("get members", func() (interface{}, error) {
			return client.Members(roomId)
		})
		if err != nil {
			continue
		}
		for _, e := range membersResp.(*mautrix.RespMembers).Chunk {
			if e.Type != mevent.StateMember {
				continue
			}
			e.Content.ParseRaw(mevent.StateMember)
			if e.Content.AsMember().Membership == mevent.MembershipJoin {
				mids = append(mids, mid.UserID(*e.StateKey))
			}
		}

		// Detect DMs
		if len(mids) == 2 {
			for _, m := range mids {
				if m != username {
					configurationRoomForUser[m] = roomId
					break
				}
			}
		}
	}
	log.Debug("DM rooms:")
	for user, room := range configurationRoomForUser {
		log.Debugf("  %s -> %s", user, room)
	}

	// create a message cache
	messageCache := cache.NewMessageCache(client, olmMachine)

	// this channel is for parsing reaction messages
	reactionChan := make(chan mevent.Event)

	// this channel is for submitting events to enable auto joining channels
	mentionChan := make(chan mevent.Event)

	// this channel is for handling membership events
	membershipChan := make(chan mevent.Event, 10)

	go workers.ReactionWorker(client, reactionChan, emojiKey, configurationRoomForUser, messageCache)
	go workers.MentionWorker(client, mentionChan, username, configurationRoomForUser, messageCache, &configuration)
	go workers.MembershipWorker(client, membershipChan, username, &configuration, joinedRooms, &roomInfoCache)

	syncer := client.Syncer.(*mautrix.DefaultSyncer)

	// Hook up the OlmMachine into the Matrix client so it receives e2ee
	// keys and other such things.
	syncer.OnSync(func(resp *mautrix.RespSync, since string) bool {
		olmMachine.ProcessSyncResponse(resp, since)
		return true
	})
	syncer.OnEvent(client.Store.(*store.QuotesfilebotStore).UpdateState)

	syncer.OnEventType(mevent.StateMember, func(_ mautrix.EventSource, event *mevent.Event) {
		olmMachine.HandleMemberEvent(event)
		membershipChan <- *event
	})

	syncer.OnEventType(mevent.StateEncryption, func(_ mautrix.EventSource, event *mevent.Event) {
		roomInfoCache.EncryptionEventCache[event.RoomID] = event.Content.AsEncryption()
	})

	syncer.OnEventType(mevent.EventReaction, func(_ mautrix.EventSource, event *mevent.Event) {
		reactionChan <- *event
	})

	// syncer.OnEventType(mevent.EventRedaction, func(_ mautrix.EventSource, event *mevent.Event) {
	// TODO (#8)
	// })

	syncer.OnEventType(mevent.EventMessage, func(_ mautrix.EventSource, event *mevent.Event) {
		mentionChan <- *event
	})

	syncer.OnEventType(mevent.EventEncrypted, func(source mautrix.EventSource, event *mevent.Event) {
		decryptedEvent, err := olmMachine.DecryptMegolmEvent(event)
		if err != nil {
			log.Warn("Failed to decrypt: ", err)
		} else {
			log.Debug("Received encrypted event: ", decryptedEvent.Content.Raw)
			if _, isMessage := decryptedEvent.Content.Parsed.(*mevent.MessageEventContent); isMessage {
				mentionChan <- *decryptedEvent
			}
		}
	})

	for {
		log.Debugf("Running sync...")
		err = client.Sync()
		if err != nil {
			log.Errorf("Sync failed. %+v", err)
		}
	}
}
