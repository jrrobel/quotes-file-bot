module gitlab.com/jrrobel/quotes-file-bot

go 1.20

require (
	github.com/PuerkitoBio/goquery v1.6.1 // indirect
	github.com/andybalholm/cascadia v1.2.0 // indirect
	github.com/btcsuite/btcd v0.21.0-beta // indirect
	github.com/btcsuite/goleveldb v1.0.0 // indirect
	github.com/btcsuite/snappy-go v1.0.0 // indirect
	github.com/decred/dcrd/lru v1.1.0 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/jessevdk/go-flags v1.5.0 // indirect
	github.com/kkdai/bstream v1.0.0 // indirect
	github.com/kyoh86/xdg v1.2.0
	github.com/mattn/go-sqlite3 v1.14.7
	github.com/mitchellh/go-wordwrap v1.0.1 // indirect
	github.com/sethvargo/go-retry v0.1.0
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20210525143221-35b2ab0089ea // indirect
	golang.org/x/term v0.0.0-20210503060354-a79de5458b56 // indirect
	golang.org/x/text v0.3.6 // indirect
	golang.org/x/tools v0.1.2 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	maunium.net/go/maulogger/v2 v2.2.5 // indirect
	maunium.net/go/mautrix v0.9.12
)
