package workers

import (
	"fmt"
	"strings"
	"sync"

	wordwrap "github.com/mitchellh/go-wordwrap"
	log "github.com/sirupsen/logrus"
	mautrix "maunium.net/go/mautrix"
	mevent "maunium.net/go/mautrix/event"
	mid "maunium.net/go/mautrix/id"

	"gitlab.com/jrrobel/quotes-file-bot/cache"
	"gitlab.com/jrrobel/quotes-file-bot/config"
	"gitlab.com/jrrobel/quotes-file-bot/helper"
)

var configRoomForUserMutex sync.Mutex

// TODO this should support encryption (#22)
func SendNoticeToUserDM(
	client *mautrix.Client,
	username mid.UserID,
	configurationRoomForUser map[mid.UserID]mid.RoomID,
	quoteString string,
	quoteAttribution string,
	quoteRoom string,
) {
	// Lock while creating the DM.
	configRoomForUserMutex.Lock()
	if _, ok := configurationRoomForUser[username]; !ok {
		log.Infof("Creating DM with %s...", username)
		r, err := helper.DoRetry("create dm", func() (interface{}, error) {
			return client.CreateRoom(&mautrix.ReqCreateRoom{
				Preset: "trusted_private_chat",
				Name:   "Quotesfilebot Archive",
				Topic:  "Configuration room for Quotesfilebot",
				Invite: []mid.UserID{username},
			})
		})
		if err != nil {
			// give up
			log.Errorf("Giving up on creating DM with %s. Error: %s", username, err)
			return
		}
		configurationRoomForUser[username] = r.(*mautrix.RespCreateRoom).RoomID
	}
	configRoomForUserMutex.Unlock()

	// Format the quote and send it to the user in a code block.
	wrappedQuote := wordwrap.WrapString(quoteString, 72)
	formattedQuote := fmt.Sprintf("%s\n    -- %s (%s)\n%%", wrappedQuote, quoteAttribution, quoteRoom)
	log.Debugf("Sending message in DM with %s.", username)
	_, err := helper.DoRetry("send message", func() (interface{}, error) {
		return client.SendMessageEvent(configurationRoomForUser[username],
			mevent.EventMessage,
			&mevent.MessageEventContent{
				MsgType:       mevent.MsgText,
				Body:          formattedQuote,
				Format:        mevent.FormatHTML,
				FormattedBody: fmt.Sprintf("<pre><code>%s</code></pre>", formattedQuote),
			})
	})
	if err != nil {
		log.Errorf("Sending message in DM with %s failed. Error: %s", username, err)
	}
}

func getSenderDisplayName(client *mautrix.Client, sender mid.UserID) string {
	d, err := helper.DoRetry("get display name", func() (interface{}, error) {
		return client.GetDisplayName(sender)
	})
	if err == nil {
		displayName := d.(*mautrix.RespUserDisplayName)
		return displayName.DisplayName
	} else {
		// fall back to sender UserID
		return sender.String()
	}
}

func getRoomName(client *mautrix.Client, roomId mid.RoomID) string {
	var roomNameResp *mevent.RoomNameEventContent
	r, err := helper.DoRetry("get room name", func() (interface{}, error) {
		err := client.StateEvent(roomId, mevent.StateRoomName, "", &roomNameResp)
		if err == nil {
			return roomNameResp.Name, err
		} else {
			return nil, err
		}
	})
	if err == nil {
		return r.(string)
	} else {
		return "unknown room"
	}
}

// This thread parses mentions.
func MentionWorker(
	client *mautrix.Client,
	mentionChan chan mevent.Event,
	username mid.UserID,
	configurationRoomForUser map[mid.UserID]mid.RoomID,
	messageCache *cache.MessageCache,
	configuration *config.Configuration,
) {
	localpart, _, err := username.ParseAndDecode()
	if err != nil {
		// this shouldn't fail because we already parsed this in the main
		panic(err)
	}

	for {
		// recieve event to parse
		event := <-mentionChan

		// Skip own events.
		if event.Sender == username {
			continue
		}

		message := event.Content.AsMessage()
		log.Debugf("Message: %+v", message)

		// The rest of the code is only applicable if this is a mention.
		messageBody := message.Body
		if !strings.Contains(messageBody, localpart) {
			continue
		}

		quoteAttribution := "Me"

		// If it's a reply, use the reply body.
		if message.RelatesTo != nil {
			log.Debugf("  %s relates to: %+s", event.ID, message.RelatesTo.EventID.String())

			quoted, err := messageCache.GetMessage(event.RoomID, message.RelatesTo.EventID)
			if err != nil {
				log.Error(err)
				continue
			}
			message = quoted.Content.AsMessage()

			if quoted.Sender != event.Sender {
				quoteAttribution = getSenderDisplayName(client, quoted.Sender)
			}
		}

		if message.MsgType != mevent.MsgText {
			continue
		}

		// Allow things such as
		//   !quotesfilebot
		//   @quotesfilebot
		//   @quotesfilebot:
		//   quotesfilebot:
		quote := message.Body
		quote = strings.TrimSpace(quote)
		quote = strings.TrimPrefix(quote, "!")
		quote = strings.TrimPrefix(quote, "@")
		quote = strings.TrimPrefix(quote, localpart)
		quote = strings.TrimPrefix(quote, ":")
		quote = strings.TrimSpace(quote)

		if strings.ToLower(quote) == "help" {
			helper.SendHelp(client, configuration, event.RoomID)
			continue
		}

		roomName := getRoomName(client, event.RoomID)
		SendNoticeToUserDM(client, event.Sender, configurationRoomForUser, quote, quoteAttribution, roomName)
	}
}

// This thread parses reaction messages.
func ReactionWorker(
	client *mautrix.Client,
	reactionChan chan mevent.Event,
	emojiKey string,
	configurationRoomForUser map[mid.UserID]mid.RoomID,
	messageCache *cache.MessageCache,
) {
	for {
		// recieve event to parse
		event := <-reactionChan

		// cast the event into ReactionEventContent
		reaction := event.Content.AsReaction()
		if reaction == nil {
			continue
		}

		// check if the reaction is the user specified emoji, otherwise ignore
		if reaction.RelatesTo.Key != emojiKey {
			continue
		}
		log.Debugf("Reaction Message: %+v", reaction)

		reactedTo, err := messageCache.GetMessage(event.RoomID, reaction.RelatesTo.EventID)
		if err != nil {
			log.Error(err)
			continue
		}
		reactedToMsg := reactedTo.Content.AsMessage()

		// if this isn't a text message, don't do anything
		if reactedToMsg.MsgType != mevent.MsgText {
			continue
		}

		var quoteAttribution string

		if reactedTo.Sender == event.Sender {
			quoteAttribution = "Me"
		} else {
			quoteAttribution = getSenderDisplayName(client, reactedTo.Sender)
		}

		// send notice to channel confirming recipt of meme submission
		roomName := getRoomName(client, event.RoomID)
		SendNoticeToUserDM(client, event.Sender, configurationRoomForUser, reactedToMsg.Body, quoteAttribution, roomName)
	}
}
