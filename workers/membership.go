package workers

import (
	log "github.com/sirupsen/logrus"
	mautrix "maunium.net/go/mautrix"
	mevent "maunium.net/go/mautrix/event"
	mid "maunium.net/go/mautrix/id"

	"gitlab.com/jrrobel/quotes-file-bot/config"
	"gitlab.com/jrrobel/quotes-file-bot/crypto"
	"gitlab.com/jrrobel/quotes-file-bot/helper"
)

// This thread parses reaction messages and determines if
func MembershipWorker(
	client *mautrix.Client,
	membershipChan chan mevent.Event,
	username mid.UserID,
	configuration *config.Configuration,
	joinedRooms map[mid.RoomID]struct{},
	roomInfoCache *crypto.RoomInfoCache,
) {
	var exists = struct{}{}
	for {
		event := <-membershipChan

		membershipEvent := event.Content.AsMember()
		log.Debugf("Membership Event: %+v", membershipEvent)
		log.Tracef("%+v", roomInfoCache)

		// Update the room membership map.
		if membershipEvent.Membership.IsInviteOrJoin() {
			if _, found := roomInfoCache.RoomMembership[event.RoomID]; !found {
				roomInfoCache.RoomMembership[event.RoomID] = make(map[mid.UserID]struct{})
			}
			roomInfoCache.RoomMembership[event.RoomID][mid.UserID(*event.StateKey)] = exists
		} else {
			delete(roomInfoCache.RoomMembership[event.RoomID], mid.UserID(*event.StateKey))
		}

		// The rest of the function is only if the membership event is
		// targeting the bot.
		if *event.StateKey != username.String() {
			continue
		}

		if membershipEvent.Membership == mevent.MembershipInvite {
			log.Debug("Bot has been invited to ", event.RoomID)
		} else if membershipEvent.Membership.IsLeaveOrBan() {
			log.Debug("Bot has been kicked from ", event.RoomID)
			delete(joinedRooms, event.RoomID)
			delete(roomInfoCache.RoomMembership, event.RoomID)
			continue
		} else {
			continue
		}

		// if the bot is already in the room then skip
		if _, joined := joinedRooms[event.RoomID]; joined {
			log.Debug("Bot is already in ", event.RoomID)
			continue
		}

		log.Info("Joining ", event.RoomID)
		_, err := helper.DoRetry("join room", func() (interface{}, error) {
			return client.JoinRoomByID(event.RoomID)
		})
		if err != nil {
			log.Error("Could not join channel %s. Error %s", event.RoomID.String(), err)
		} else {
			log.Infof("Joined %s sucessfully", event.RoomID.String())
			joinedRooms[event.RoomID] = exists
			helper.SendHelp(client, configuration, event.RoomID)
		}
	}
}
