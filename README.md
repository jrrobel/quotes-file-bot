# quotesfilebot

Very, very WIP. Don't use. There are major issues.

A bot that helps you save quotes from Matrix in a
[fortune-mod](https://github.com/shlomif/fortune-mod) quotesfile.

Public instance at
[@quotesfilebot:nevarro.space](https://matrix.to/#/@quotesfilebot:nevarro.space).

## Usage

1. Invite the quotesfilebot to your room.

1. Use one of the following methods to log a quote:

    * Mention it as the first part of the message. For example:

      ```
      @quotesfilebot: this is an awesome quote
      ```

    * Mention the bot on a reply to another message. For example:

    * React with the quotesfilebot emoji (from config)

1. The quotesfilebot will start a DM or use an existing one to send you a
   formatted quote that you can copy into your quotesfile.

## Roadmap

- [x] Basic functionality
  - [x] Mentions
  - [x] Replies
  - [x] Reactions
- [ ] E2EE support
- [ ] Support for user-configured emoji reaction
- [ ] Support for user-configured automatic PR/MR/patch email generation

## Configuration

The configuration file is a JSON file which must have the following fields:

* `DefaultReactionEmoji` - the emoji t

Example
```
{
    "DefaultReactionEmoji": "💬",
    "Username": "@quotesfilebot:example.com",
    "PasswordFile": "/path/to/a/file/with/your/password",
    "Homeserver": "https://matrix.example.com",
    "JoinMessage": "I'm a quotesfilebot!"
}
```

## Development

It's a standard Go project. The normal things work:

* `go build` for building the project
* `go run . -c config.json` for running the project
