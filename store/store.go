package store

import (
	"encoding/json"
	"sync"

	"maunium.net/go/mautrix"
	mevent "maunium.net/go/mautrix/event"
	mid "maunium.net/go/mautrix/id"
)

type QuotesfilebotStore struct {
	sync.RWMutex
	AccessToken string                       `json:"access_token"`
	Filters     map[mid.UserID]string        `json:"filters"`
	BatchTokens map[mid.UserID]string        `json:"batch_tokens"`
	Rooms       map[mid.RoomID]*mautrix.Room `json:"rooms"`
}

func NewQuotesfilebotStore() *QuotesfilebotStore {
	return &QuotesfilebotStore{
		AccessToken: "",
		Filters:     make(map[mid.UserID]string),
		BatchTokens: make(map[mid.UserID]string),
		Rooms:       make(map[mid.RoomID]*mautrix.Room),
	}
}

func (s *QuotesfilebotStore) UnmarshalJSON(b []byte) error {
	type tmpStore QuotesfilebotStore
	storetmp := &tmpStore{
		AccessToken: "",
		Filters:     make(map[mid.UserID]string),
		BatchTokens: make(map[mid.UserID]string),
		Rooms:       make(map[mid.RoomID]*mautrix.Room),
	}

	if err := json.Unmarshal(b, storetmp); err != nil {
		return err
	}
	*s = QuotesfilebotStore(*storetmp)
	return nil
}

func (s *QuotesfilebotStore) SaveFilterID(userID mid.UserID, filterID string) {
	s.RLock()
	defer s.RUnlock()
	s.Filters[userID] = filterID
}

func (s *QuotesfilebotStore) LoadFilterID(userID mid.UserID) string {
	s.RLock()
	defer s.RUnlock()
	return s.Filters[userID]
}

func (s *QuotesfilebotStore) SaveNextBatch(userID mid.UserID, nextBatchToken string) {
	s.RLock()
	defer s.RUnlock()
	s.BatchTokens[userID] = nextBatchToken
}

func (s *QuotesfilebotStore) LoadNextBatch(userID mid.UserID) string {
	s.RLock()
	defer s.RUnlock()
	return s.BatchTokens[userID]
}

func (s *QuotesfilebotStore) SaveRoom(room *mautrix.Room) {
	s.RLock()
	defer s.RUnlock()
	s.Rooms[room.ID] = room
}

func (s *QuotesfilebotStore) LoadRoom(roomID mid.RoomID) *mautrix.Room {
	s.RLock()
	defer s.RUnlock()
	return s.Rooms[roomID]
}

func (s *QuotesfilebotStore) UpdateState(_ mautrix.EventSource, event *mevent.Event) {
	if !event.Type.IsState() {
		return
	}
	s.RLock()
	defer s.RUnlock()

	room := s.LoadRoom(event.RoomID)
	if room == nil {
		room = mautrix.NewRoom(event.RoomID)
		s.SaveRoom(room)
	}
	room.UpdateState(event)
}
