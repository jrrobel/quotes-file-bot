package helper

import (
	"fmt"
	_ "strconv"
	"time"

	"github.com/sethvargo/go-retry"
	log "github.com/sirupsen/logrus"
	mautrix "maunium.net/go/mautrix"
	mid "maunium.net/go/mautrix/id"

	config "gitlab.com/jrrobel/quotes-file-bot/config"
)

func DoRetry(description string, fn func() (interface{}, error)) (interface{}, error) {
	b, err := retry.NewFibonacci(1 * time.Second)
	if err != nil {
		panic(err)
	}
	b = retry.WithMaxRetries(5, b)
	for {
		log.Info("trying: ", description)
		val, err := fn()
		if err == nil {
			log.Info(description, " succeeded")
			return val, nil
		}
		nextDuration, stop := b.Next()
		log.Debugf("  %s failed. Retrying in %d seconds...", description, nextDuration)
		if stop {
			log.Debugf("  %s failed. Retry limit reached. Will not retry.", description)
			break
		}
		time.Sleep(nextDuration)
	}
	return nil, err
}

func SendHelp(
	client *mautrix.Client,
	configuration *config.Configuration,
	roomID mid.RoomID,
) {
	// send message to channel confirming join (retry 3 times)
	DoRetry("send help", func() (interface{}, error) {
		noticeText := fmt.Sprintf(
			"%s\nTo submit a quote, mention the bot or react with %s.",
			configuration.JoinMessage,
			configuration.DefaultReactionEmoji)
		return client.SendNotice(roomID, noticeText)
	})
}
